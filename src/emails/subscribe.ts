import superagent from 'superagent'
import { DietRequestedEvent } from '../diet-requests'

export const subscribeToMailingList = async (apiKey: string, input: DietRequestedEvent): Promise<void> => {
  const { email, givenName, dailyCalorieIntake, calculatorInput } = input
  const { gender, height, weight, age, goal, activityLevel } = calculatorInput

  await superagent
    .post('https://api.mailerlite.com/api/v2/subscribers')
    .set('X-MailerLite-ApiKey', apiKey)
    .send({
      email,
      fields: {
        dailycaloriesintake: dailyCalorieIntake,
        gender,
        height,
        weight,
        age,
        goal,
        source: 'calorie_calculator_form',
        'activity_level': activityLevel,
        'given_name': givenName,
      },
      resubscribe: true
    })
}