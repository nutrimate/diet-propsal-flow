
export type Recipe = {
  id: string;
  name: string;
  instructions: string;
  timeToPrepare: string;
  calories: number;
  proteins: number;
  fats: number;
  carbohydrates: number;
}

export type DietEntry = {
  day: number;
  recipes: (Recipe & { meal: string })[];
  summary: {
    calories: number;
    proteins: number;
    fats: number;
    carbohydrates: number;
  };
}

export type Diet = DietEntry[]
