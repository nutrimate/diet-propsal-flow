import AWS from 'aws-sdk'
import { Diet } from './models'

export const storeDiet = async (
  s3Client: AWS.S3,
  bucketName: string,
  requestId: string,
  diet: Diet,
): Promise<string> => {
  const key = `${requestId}/diet.json`

  await s3Client.putObject({
    Bucket: bucketName,
    Body: JSON.stringify(diet),
    Key: key,
  }).promise()

  return key
}
