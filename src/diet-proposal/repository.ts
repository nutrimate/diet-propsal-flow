import recipes from './recipes.json'
import { Recipe } from './models'

export class RecipeRepository {
  private readonly recipesById: { [id: string]: Recipe | undefined }

  constructor () {
    this.recipesById = recipes
      .map(({ meals, ...recipe }) => recipe)
      .reduce(
        (byId, recipe) => ({ ...byId, [recipe.id]: recipe }),
        {} as { [id: string]: Recipe },
      )
  }

  findById (recipeId: string): Recipe {
    const recipe = this.recipesById[recipeId]

    if (!recipe) {
      throw new Error(`Could not find recipe with id: ${recipeId}`)
    }

    return recipe
  }
}
