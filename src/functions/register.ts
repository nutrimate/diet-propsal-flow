/// <reference types="aws-lambda" />
import AWS from 'aws-sdk'
import { DietRequestedEvent, DietRequest, DietRequestStore } from '../diet-requests'
import { getNotificationSubject, userRegisteredNotification, OutputNotification } from '../notifications'
import { sentryHandler, requiredEnv } from '../infrastructure'
import { subscribeToMailingList } from '../emails'

type StoreDietRequestResult = {
  result: DietRequest;
  notification: OutputNotification;
}

const config = {
  env: requiredEnv('ENV'),
  requestsTableName: requiredEnv('REQUESTS_TABLE_NAME'),
  mailerApiKey: process.env.MAILER_API_KEY || '',
}

export const handler = sentryHandler(
  async (event: DietRequestedEvent): Promise<StoreDietRequestResult> => {
    const documentClient = new AWS.DynamoDB.DocumentClient({
      convertEmptyValues: true,
    })
    const store = new DietRequestStore(documentClient, config.requestsTableName)

    if (config.env === 'production') {
      await subscribeToMailingList(config.mailerApiKey, event)
    }

    const dietRequest = await store.save(event)

    return {
      result: dietRequest,
      notification: {
        default: JSON.stringify(dietRequest),
        subject: getNotificationSubject(config.env, event.email),
        email: userRegisteredNotification(event.email, dietRequest),
      },
    }
  }
)