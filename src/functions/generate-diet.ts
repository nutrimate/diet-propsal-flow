/// <reference types="aws-lambda" />
import AWS from 'aws-sdk'
import { sentryHandler, requiredEnv } from '../infrastructure'
import { getNotificationSubject, dietGeneratedNotification } from '../notifications'
import { createMealPlannerApiClient } from '../meal-planning'
import { RecipeRepository, storeDiet } from '../diet-proposal'

type GenerateDietEvent = {
  requestId: string;
  email: string;
  dailyCalorieIntake: number;
  calculatorInput: {
    weight: number;
    goal: string;
  };
}

type GenerateDietResult = {
  result: {
    dietLocation: string;
  };
  notification: {
    subject: string;
    default: string;
    email: string;
  };
}

const config = {
  env: requiredEnv('ENV'),
  outputBucketName: requiredEnv('OUTPUT_BUCKET_NAME'),
  mealPlannerBaseUrl: requiredEnv('MEAL_PLANNER_BASE_URL'),
  websiteUrl: requiredEnv('WEBSITE_URL')
}

export const handler = sentryHandler(
  async (event: GenerateDietEvent): Promise<GenerateDietResult> => {
    const { requestId } = event
    const s3Client = new AWS.S3()
    const recipeRepository = new RecipeRepository()
    const mealPlannerApiClient = createMealPlannerApiClient(
      config.mealPlannerBaseUrl,
      recipeRepository,
    )

    const diet = await mealPlannerApiClient.planDiet({
      dailyCalorieIntake: event.dailyCalorieIntake,
      goal: event.calculatorInput.goal,
      weight: event.calculatorInput.weight,
    })
    const dietLocation = await storeDiet(s3Client, config.outputBucketName, requestId, diet)
    const result = { dietLocation }

    return {
      result,
      notification: {
        default: JSON.stringify(result),
        subject: getNotificationSubject(config.env, event.email),
        email: dietGeneratedNotification(config.websiteUrl, requestId),
      }
    }
  }
)
