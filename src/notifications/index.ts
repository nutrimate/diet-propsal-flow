import { DietRequest } from '../diet-requests'

export type OutputNotification = {
  default: string;
  email: string;
  subject?: string;
}

export const getNotificationSubject = (env: string, email: string): string => {
  const prefix = env === 'production' ? 'CloudWeight' : `${env} - CloudWeight`
  return `${prefix} - New contact: ${email}`
}

export const userRegisteredNotification = (email: string, request: DietRequest): string => [
  'Hi 👋,',
  `user with email: ${email} has requested a diet! 🎉`,
  `This request has automatically been assigned an id of: ${request.requestId}`,
  '',
  `Raw data:`,
  `${JSON.stringify(request, null, 2)}`,
  '',
  `Congrats,`,
  `🤖🤖🤖`,
].join('\n')

export const dietGeneratedNotification = (websiteUrl: string, requestId: string): string => [
  `Diet has been generated for request ${requestId}.`,
  `You can view the diet here: ${websiteUrl}/diet/${requestId}`,
  '',
  `Good luck,`,
  `🤖🤖🤖`,
].join('\n')
