export type DietRequest = {
  requestId: string;
  givenName: string;
  email: string;
  dailyCalorieIntake: number;
  calculatorInput: object;
}

export type DietRequestedEvent = {
  email: string;
  givenName: string;
  dailyCalorieIntake: number;
  calculatorInput: {
    height: number;
    weight: number;
    age: number;
    goal: string;
    activityLevel: string;
    gender: string;
  };
}