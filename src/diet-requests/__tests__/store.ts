import AWS from 'aws-sdk'
import { DietRequestStore, SaveDietRequestCommand } from '../store'

describe('DietRequestStore', () => {
  const createDocumentClientMock = (): jest.Mocked<AWS.DynamoDB.DocumentClient> => {
    const mock: Partial<AWS.DynamoDB.DocumentClient> = {
      put: jest.fn().mockReturnValue({ promise: () => Promise.resolve() })
    }

    return mock as any
  }

  it('should save request', async () => {
    const documentClientMock = createDocumentClientMock()
    const tableName = 'test'
    const store = new DietRequestStore(documentClientMock, tableName)
    const command: SaveDietRequestCommand = {
      email: 'foo@bar.com',
      givenName: 'John',
      calculatorInput: {
        weight: 80,
      },
      dailyCalorieIntake: 2000,
    }

    await store.save(command)

    expect(documentClientMock.put).toHaveBeenCalledWith(expect.objectContaining({
      TableName: tableName,
      Item: {
        requestId: expect.any(String),
        timestamp: expect.any(String),
        calculatorInput: command.calculatorInput,
        givenName: command.givenName,
        email: command.email,
        dailyCalorieIntake: command.dailyCalorieIntake
      }
    }))
  })
})