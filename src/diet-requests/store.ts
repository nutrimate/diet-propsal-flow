import AWS from 'aws-sdk'
import { v1 } from 'uuid'
import { DietRequest } from './model'

export type SaveDietRequestCommand = {
  email: string;
  givenName: string;
  calculatorInput: object;
  dailyCalorieIntake: number;
}

export class DietRequestStore {
  constructor (
    private readonly documentClient: AWS.DynamoDB.DocumentClient,
    private readonly tableName: string,
  ) {
  }

  async save (command: SaveDietRequestCommand): Promise<DietRequest> {
    const request = {
      requestId: v1(),
      timestamp: new Date().toISOString(),
      email: command.email,
      givenName: command.givenName,
      calculatorInput: command.calculatorInput,
      dailyCalorieIntake: command.dailyCalorieIntake,
    }
  
    await this.documentClient.put({
      Item: request,
      TableName: this.tableName,
      ConditionExpression: 'attribute_not_exists(requestId)'
    }).promise()
  
    return request
  }
}