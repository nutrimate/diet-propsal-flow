import { init, NodeOptions, captureException, flush } from '@sentry/node'

type Handler<T extends unknown [], R> = (...args: T) => Promise<R>

export const sentryHandler = <T extends unknown[], R>(handler: Handler<T, R>, options?: NodeOptions): Handler<T, R> => {
  init(options)

  return async (...args: T): Promise<R> => {
    try {
      return await handler(...args)
    } catch (error) {
      captureException(error)
      await flush(2000)
      throw error
    }
  }
}
