import superagent from 'superagent'
import { groupBy, toPairs, sum, map, sortBy, pipe } from 'ramda'
import { Diet, DietEntry, RecipeRepository } from '../diet-proposal'

export type GeneratedDietEntry = { day: number; recipeId: string; meal: string }

export type PlanDietOptions = {
  dailyCalorieIntake: number;
  weight: number;
  goal: string;
}

export type MealPlannerApiClient = {
  planDiet: (options: PlanDietOptions) => Promise<Diet>;
}

const defaultNumberOfDays = 7
const defaultMealSchedule = [
  'Breakfast',
  'Dinner',
  'Supper'
]

const mapDietToOutput = (repository: RecipeRepository, diet: GeneratedDietEntry[]): Diet => pipe(
  groupBy<GeneratedDietEntry>(entry => entry.day.toString()),
  toPairs,
  map(([day, dietEntries]: [string, GeneratedDietEntry[]]): DietEntry => {
    const recipes = dietEntries.map(({ recipeId, meal }) => {
      const recipe = repository.findById(recipeId)
      return { ...recipe, meal }
    })

    return {
      day: Number(day),
      recipes: sortBy(r => defaultMealSchedule.indexOf(r.meal))(recipes),
      summary: {
        calories: sum(recipes.map(r => r.calories)),
        proteins: sum(recipes.map(r => r.proteins)),
        fats: sum(recipes.map(r => r.fats)),
        carbohydrates: sum(recipes.map(r => r.carbohydrates)),
      }
    }
  }),
)(diet)

export const createMealPlannerApiClient = (mealPlannerBaseUrl: string, repository: RecipeRepository): MealPlannerApiClient => ({
  planDiet: options => {
    const payload = {
      mealSchedule: defaultMealSchedule,
      days: defaultNumberOfDays,
      ...options,
    }

    return superagent
      .post(`${mealPlannerBaseUrl}/plan-diet`)
      .send(payload)
      .then(r => {
        const { status, result } = r.body

        if (status !== "success") {
          const error = new Error("Meal planning failed")

          throw Object.assign(error, { status, payload, result })
        }

        return result
      })
      .then(generatedDiet => mapDietToOutput(repository, generatedDiet))
  }
})
